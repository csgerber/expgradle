package edu.uchicago.cs.quiz.pkuprys;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Random;

public class QuestionActivity extends Activity {
    public static final String QUESTION = "edu.uchicago.cs.quiz.pkuprys.QUESTION";

    private static final String DELIMITER = "\\|";
    private static final int NUM_ANSWERS = 5;
    private static final int COUNTRY = 0;
    private static final int CAPITAL = 1;
    private static final int REGION = 2;

    Random mRandom;

    private Question mQuestion;
    private String[] mCountriesCapitals;
    private boolean mItemSelected = false;
    //make these members
    TextView mQuestionNumberTextView;
    RadioGroup mQuestionRadioGroup;
    TextView mQuestionTextView;
    Button mSubmitButton;
    Button mQuitButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_question);

        //generate a question
        mCountriesCapitals = getResources().getStringArray(R.array.countries_capitals);

        //get refs to inflated members
        mQuestionNumberTextView = (TextView) findViewById(R.id.questionNumber);
        mQuestionTextView = (TextView) findViewById(R.id.questionText);
        mSubmitButton = (Button) findViewById(R.id.submitButton);

        //init the random
        mRandom = new Random();

        mSubmitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                submit();
            }
        });


        //set quit button action
        mQuitButton = (Button) findViewById(R.id.quitButton);
        mQuitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                displayResult();
            }
        });

        mQuestionRadioGroup = (RadioGroup) findViewById(R.id.radioAnswers);
        //disallow submitting until an answer is selected
        mQuestionRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                mSubmitButton.setEnabled(true);
                mItemSelected = true;
            }
        });

        fireQuestion(savedInstanceState);
    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        //pass the question into the bundle when I have a config change
        outState.putSerializable(QuestionActivity.QUESTION, mQuestion);
    }


    private void fireQuestion(){
        mQuestion = getQuestion();
        populateUserInterface();
    }

    //overloaded to take savedInstanceState
    private void fireQuestion(Bundle savedInstanceState){

        if (savedInstanceState == null ){
            mQuestion = getQuestion();
        } else {
            mQuestion = (Question) savedInstanceState.getSerializable(QuestionActivity.QUESTION);
        }

        populateUserInterface();

    }

    private void populateUserInterface() {
        //take care of button first
        mSubmitButton.setEnabled(false);
        mItemSelected = false;

        //populate the QuestionNumber textview
        String questionNumberText = getResources().getString(R.string.questionNumberText);
        int number = QuizTracker.getInstance().getQuestionNum();
        mQuestionNumberTextView.setText(String.format(questionNumberText, number));

        //set question text
        mQuestionTextView.setText(mQuestion.getQuestionText());

        //will generate a number 0-4 inclusive
        int randomPosition = mRandom.nextInt(NUM_ANSWERS);
        int counter = 0;
        mQuestionRadioGroup.removeAllViews();
        //for each of the 5 wrong answers
        for (String wrongAnswer : mQuestion.getWrongAnswers()) {
            if (counter == randomPosition) {
                //insert the cor answer
                addRadioButton(mQuestionRadioGroup, mQuestion.getCapital());
            } else {
                addRadioButton(mQuestionRadioGroup, wrongAnswer);
            }
            counter++;
        }
    }


    private void submit() {

        Button checkedButton = (Button) findViewById(mQuestionRadioGroup.getCheckedRadioButtonId());
        String guess = checkedButton.getText().toString();
        //see if they guessed right
        if (mQuestion.getCapital().equals(guess)) {
            QuizTracker.getInstance().answeredRight();
        } else {
            QuizTracker.getInstance().answeredWrong();
        }
        if (QuizTracker.getInstance().getTotalAnswers() < Integer.MAX_VALUE) {
            //increment the question number
            QuizTracker.getInstance().incrementQuestionNumber();
            fireQuestion();
        } else {
            displayResult();
        }

     }

    private void displayResult(){
        Intent intent = new Intent(this, ResultActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_question, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menuQuit:
                displayResult();
                return true;
            case R.id.menuSubmit:
                if(mItemSelected){
                submit();
                }
                else{
                    Toast toast = Toast.makeText(this, getResources().getText(R.string.pleaseSelectAnswer), Toast.LENGTH_SHORT);
                    toast.show();
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void addRadioButton(RadioGroup questionGroup, String text) {
        RadioButton button = new RadioButton(this);
        button.setText(text);
        button.setTextColor(Color.WHITE);
        questionGroup.addView(button);

    }

    private Question getQuestion() {
        //generate corr answer
        String[] answer = getRandomCountryCapital();
        mQuestion = new Question(answer[COUNTRY], answer[CAPITAL], answer[REGION]);

        //generates 5 wrong answers
        while (mQuestion.getWrongAnswers().size() < NUM_ANSWERS) {
            String[] countryCapital = getRandomCountryCapital();

            //if the one we picked is equal to the answer OR
            //if is not from the same region as the answer OR
            //if we already picked this one
            while (countryCapital[CAPITAL].equals(answer[CAPITAL]) ||
                    !countryCapital[REGION].equals(answer[REGION]) ||
                    mQuestion.getWrongAnswers().contains(countryCapital[CAPITAL])) {
                //then we need pick another one
                countryCapital = getRandomCountryCapital();
            }

            mQuestion.addWrongAnswer(countryCapital[CAPITAL]);
        }
        return mQuestion;
    }

    public Question getTheQuestion(){
        return mQuestion;
    }

    private String[] getRandomCountryCapital() {
        int index = mRandom.nextInt(mCountriesCapitals.length);
        return mCountriesCapitals[index].split(DELIMITER);
    }
}
